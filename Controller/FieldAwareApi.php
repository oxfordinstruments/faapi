<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 12:14 PM
 */

namespace Controller;

use ProgressBar\Manager;
use Repository\ItemNew;
use Repository\Job;

class FieldAwareApi
{
	private $apiKey;
	private $baseUrl;
	private $pageSize = 20;
	private $debug = false;
	private $showCustomFields = false;
	private $ch;
	private $apiCalls = 0;
	private $defaultUserEmail;
	private $defaultTaskUUID;
	private $defaultPartsTaskUUID;
	private $defaultItemUUID;
	private $defaultPmQ1UUID;
	private $defaultPmQ2UUID;
	private $defaultPmQ3UUID;
	private $defaultPmQ4UUID;
	private $firstRun = true;
	private $jobsFileSizeMax;
	private $partTrackingNumberStart;
	private $partTrackingNumberPrefix;
	private $jobsToExecute;
	private $itemsToExecute;
	private $maxItemsJobsAllowedToExecute;
	private $minutesBetweenBlocks;
	private $speedLimitCounter = 1;


	public function __construct($apiKey, $baseUrl)
	{
		$this->apiKey = $apiKey;
		$this->baseUrl = $baseUrl;

		$this->curlInit();

	}

	public function __destruct()
	{
		if($this->debug){
			echo PHP_EOL,"Api calls made: ".$this->apiCalls,PHP_EOL;
		}

		$this->curlClose();
	}

	private function curlInit()
	{
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
	}

	private function curlClose()
	{
		curl_close($this->ch);
	}

	public function putData(array $json, string $url)
	{
		$apiUrl = $this->baseUrl.$url;
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token '.$this->apiKey,
			'Content-Type: application/json',
			'Content-Length: ' . strlen(json_encode($json, JSON_UNESCAPED_SLASHES))
		));
		curl_setopt($this->ch, CURLOPT_URL,$apiUrl);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($json, JSON_UNESCAPED_SLASHES));
		$rtnData = json_decode(curl_exec($this->ch), true);
		$this->apiCalls++;
		if($this->debug > 2){echo print_r($rtnData),PHP_EOL;}
		$rtn = curl_getinfo($this->ch);
		if($rtn['http_code'] == 201){
			if(isset($rtnData['jobId'])){
				return $rtnData['jobId'].' - '.$rtnData['uuid'];
			}
			return $rtnData['uuid'];
		}
		return ['REQUEST' => json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), "RETURN HEADERS" => $rtn, "RESPONSE" => $rtnData];
	}

	public function deleteJobs(array $json)
	{
		foreach ($json as $key => $data) {
			$this->apiCalls++;
			echo $data['link']['url'],PHP_EOL;
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Token '.$this->apiKey,
				'Content-Type: application/json'
			));
			curl_setopt($this->ch, CURLOPT_URL, $data['link']['url']);
			curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			$result = curl_exec($this->ch);
			$rtn = curl_getinfo($this->ch);
			if($rtn['http_code'] != 204 and $rtn['http_code'] != 404){
				echo "ERROR: HTTP CODE: ".$rtn['http_code']." JOB ID: ".$data['jobId']." RESULT: ".print_r($result),PHP_EOL;
			}
		}
	}

	/**
	 * @param string $url
	 * @param string $args
	 * @return bool
	 * @throws \Exception
	 */
	public function getData($url, $args = "")
	{
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token '.$this->apiKey,
			'Accept-Type: application/json'
		));

		echo "Getting ".$url."...",PHP_EOL;
		$pages = 0;
		$curPage = 0;
		$data = [];

		$url = str_replace("\\", "/", $url);
		$fname = $this->getFilename($url, ['url' => true]);

		if($args != ""){
			$lastArgs="&page=".$curPage."&pageSize=".$this->pageSize;
		}else{
			$lastArgs="?page=".$curPage."&pageSize=".$this->pageSize;
		}

		$apiUrl = $this->baseUrl.$url.$args.$lastArgs;
		curl_setopt($this->ch, CURLOPT_URL,$apiUrl);
		$this->apiCalls++;
		if($ret = curl_exec($this->ch)){
			$ret = json_decode($ret, true);
		}else{
			throw new \Exception('cURL Failed to return');
		}
		if(!isset($ret['count'])){
			throw new \Exception('Json return was invalid');
		}
		$pages = ceil($ret['count'] / $this->pageSize);
		foreach ($ret['items'] as $key => $item){
//			echo print_r($key, true), print_r($item, true);
			array_push($data, $item);
//			$data[$key] = ["partNumber" => $item["partNumber"], "link" => $item["link"], "uuid" => $item["uuid"], "name" => $item["name"]] ;
		}

		$pbar = new Manager(1, $pages);

		for ($curPage = 1; $curPage < $pages; $curPage++){
			if($args != ""){
				$lastArgs="&page=".$curPage."&pageSize=".$this->pageSize;
			}else{
				$lastArgs="?page=".$curPage."&pageSize=".$this->pageSize;
			}

			$apiUrl = $this->baseUrl.$url.$args.$lastArgs;
			curl_setopt($this->ch, CURLOPT_URL,$apiUrl);
			$this->apiCalls++;
			if($ret = curl_exec($this->ch)){
				$ret = json_decode($ret, true);
			}else{
				throw new \Exception('cURL Failed to return');
			}

			foreach ($ret['items'] as $item){
				array_push($data, $item);
			}
			$pbar->advance();
		}
//		echo $this->debug ? print_r($data, true):'',PHP_EOL;die();
		$this->saveData($data, $fname);
//		file_put_contents(__DIR__ . '/../Data/'.$fname, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

		return true;
	}

	public function getDataDetailAll($jsonFile)
	{
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token '.$this->apiKey,
			'Accept-Type: application/json'
		));

		echo "Getting details: ".$this->getFilename($jsonFile)."...",PHP_EOL;
		$data = [];
		$items = $this->loadData($jsonFile);
		$pbar = new Manager(0, count($items));
		if($this->debug > 2){echo "Getting Detail ".$jsonFile."...",PHP_EOL;}
		foreach ($items as $key=>$item){
//			if($this->debug > 2){echo "ITEM/KEY:";echo print_r($key,true),PHP_EOL;}


			$apiUrl = $item['link']['url'];
			if($this->debug > 2){echo "Detail URL: ".$apiUrl;}
			curl_setopt($this->ch, CURLOPT_URL,$apiUrl);
			$this->apiCalls++;
			if($ret = curl_exec($this->ch)){
				$ret = json_decode($ret, true);
			}else{
				throw new \Exception('cURL Failed to return');
			}
			array_push($data, $ret);
			$pbar->advance();
		}

		if($this->debug > 2){echo 'getDataDetailAll $data:',PHP_EOL; echo print_r($data, true),PHP_EOL;}
		$this->saveData($data, $this->getFilename($jsonFile, ['detail' => true]));

		return true;
	}

	function loadCustomFields($noShow = false)
	{
		$entityClass = [
			'Job',
			'Asset',
			'Invoice',
			'Supplier',
			'Customer',
			'Task',
			'Item',
			'Location',
			'Contact',
			'User'
		];
		sort($entityClass);

		$cfi = $this->loadData('settings_customfields');
		$cfo = [];
		foreach ($cfi as $v){
			$cfo[$v['uuid']] = $v;
		}

		if($noShow){
			return $cfo;
		}

		if($this->showCustomFields){
			$f = fopen(__DIR__."/../Data/custom_fields.txt", "w+");
			$c = new Colors();
			if($this->debug > 1){echo $c->getColoredString('Custom Fields'.PHP_EOL, INFO);}
			fwrite($f, "Custom Fields:\r\n\r\n");
			foreach ($entityClass as $ec){
				if($this->debug > 1){echo $c->getColoredString( $ec.":".PHP_EOL, 'green');}
				fwrite($f, $ec.":\r\n");
				$tmp = [];
				foreach ($cfo as $field){
					$str = "\t".str_pad($field['name'],32,' ').$field['uuid']."\t{".$field['type'].'}';
					if(strtolower($field['entityClass']) == strtolower($ec)){
						if(isset($field['options'])){
							$str .= " ".implode(", ",$field['options']);
						}
						array_push($tmp, $str);
					}
				}
				sort($tmp);
				foreach ($tmp as $t){
					if($this->debug > 1){echo $c->getColoredString($t.PHP_EOL, INFO);}
					fwrite($f, $t."\r\n");
				}
				fwrite($f, "\r\n");
				if($this->debug > 1){echo PHP_EOL;}
			}
			fclose($f);
		}


		echo "Custom Fields Array: ",PHP_EOL;
		echo print_r($cfo,true);
		return $cfo;
	}

	/**
	 * @param string $dataFile
	 * @param bool $detail
	 * @return array | boolean
	 * @throws \Exception
	 */
	public function loadData($dataFile, $detail = false)
	{
		$dataFile = $this->getFilename($dataFile, ['detail' => $detail]);
		if(!file_exists(__DIR__ . '/../Data/'.$dataFile)){
			return array();
		}
		return json_decode(file_get_contents(__DIR__ . '/../Data/'.$dataFile), true);
	}

	public function saveData(array $data, string $dataFile)
	{
		if(!preg_match("/\.json$/",strtolower($dataFile))){
			$dataFile .= ".json";
		}
		$data = [$data];
		$dir = __DIR__.'/../Data/';

		file_put_contents($dir.$dataFile, json_encode($data[0], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
		return null;
	}

	public function saveJobData(Job $data)
	{


		$data = [$data];
		$dir = __DIR__.'/../Data/Jobs/';

		if($this->firstRun){
			foreach (glob(__DIR__."/../Data/Jobs/*.json") as $filename) {
				unlink($filename);
			}
			$this->setFirstRun(false);

			file_put_contents($dir.'jobs_1.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
			return null;
		}

		$largestNum = 0;
		$largest = '';
		foreach (glob($dir."*.json") as $filename) {
			preg_match('/(.*_)(\d*)(\.json)/',basename($filename), $matches);
			if(intval($matches[2]) > $largestNum){
				$largest = $matches[0];
				$largestNum = intval($matches[2]);
			}
		}
		$file =  'jobs_'.intval($largestNum).'.json';
		clearstatcache();
		if(filesize($dir.$file) >= $this->getJobsFileSizeMax()){
			$file =  'jobs_'.(intval($largestNum)+1).'.json';
		}else{
			$odata = json_decode(file_get_contents($dir.$file), true);
			if(!is_array($odata)){
				echo print_r(file_get_contents($dir.$file)),PHP_EOL;
				echo print_r($odata),PHP_EOL;
				die(json_last_error_msg());
			}
			if($this->debug > 2){echo "JOB DATA: ", print_r($data, true),PHP_EOL;}
			$data = array_merge($odata, $data);

		}

		file_put_contents($dir.$file, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
		return null;
	}

	public function loadJobData(string $dataFile)
	{
		if(!file_exists(__DIR__.'/../Data/Jobs/'.$dataFile)){
			throw new \Exception('File '.$dataFile.' not found!');
		}
		return json_decode(file_get_contents(__DIR__ . '/../Data/Jobs/'.$dataFile), true);

	}

	public function saveItemData(ItemNew $data)
	{
		$data = [$data];
		$dir = __DIR__.'/../Data/Items/';

		if($this->firstRun){
			foreach (glob(__DIR__."/../Data/Items/*.json") as $filename) {
				unlink($filename);
			}
			$this->setFirstRun(false);

			file_put_contents($dir.'items_1.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
			return null;
		}

		$largestNum = 0;
		$largest = '';
		foreach (glob($dir."*.json") as $filename) {
			preg_match('/(.*_)(\d*)(\.json)/',basename($filename), $matches);
			if(intval($matches[2]) > $largestNum){
				$largest = $matches[0];
				$largestNum = intval($matches[2]);
			}
		}
		$file =  'items_'.intval($largestNum).'.json';
		clearstatcache();
		if(filesize($dir.$file) >= $this->getJobsFileSizeMax()){
			$file =  'items_'.(intval($largestNum)+1).'.json';
		}else{
			$odata = json_decode(file_get_contents($dir.$file), true);
			$data = array_merge($odata, $data);
			if($this->debug > 2){echo print_r($data, true),PHP_EOL;}
		}

		file_put_contents($dir.$file, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
		return null;
	}

	public function loadItemData(string $dataFile)
	{
		if(!file_exists(__DIR__.'/../Data/Items/'.$dataFile)){
			throw new \Exception('File '.$dataFile.' not found!');
		}
		return json_decode(file_get_contents(__DIR__ . '/../Data/Items/'.$dataFile), true);

	}

	public function saveJsonFile(string $fullFilePath, array $jsonArray)
	{
		if(!file_exists($fullFilePath)){
			throw new \Exception("File $fullFilePath does note exist!");
		}
		file_put_contents($fullFilePath, json_encode($jsonArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
	}

	public function getFilename($str, $params = ['detail'=>false, 'url'=>false])
	{
		if(count($params) == 0){
			throw new \Exception('getFilename invalid params');
		}

		if(isset($params['url']) and $params['url']){
			$str = str_replace("\\", "/", $str);
			$str = rtrim(preg_replace("/\//",'_',$str), "_");
		}
		if(!preg_match("/\.json$/",strtolower($str))){
			$str .= ".json";
		}
		if(isset($params['detail']) and $params['detail']){
			$str = str_replace(".json", '_detail.json', $str);
		}
		return $str;
	}

	public function saveTrackingNumberLast(string $trk)
	{
		file_put_contents(__DIR__.'/../Data/trackingLast.json', json_encode(['LastUsedTrackingNumber' => $trk], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
	}

	public function loadTrackingNumberLast()
	{
		if(!file_exists(__DIR__ . '/../Data/trackingLast.json')){
			return false;
		}
		$data = json_decode(file_get_contents(__DIR__ . '/../Data/trackingLast.json'), true);
		return $data['LastUsedTrackingNumber'];
	}

	public function saveTrackingNumberUsed(array $trk)
	{
		file_put_contents(__DIR__.'/../Data/trackingUsed.json', json_encode($trk, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
	}

	public function loadTrackingNumberUsed()
	{
		if(!file_exists(__DIR__ . '/../Data/trackingUsed.json')){
			return array();
		}
		return json_decode(file_get_contents(__DIR__ . '/../Data/trackingUsed.json'), true);
	}

	public function saveCompletedJobs(array $data)
	{
		if(!file_exists(__DIR__ . '/../Data/jobs.json')){
			file_put_contents(__DIR__.'/../Data/jobs.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
			return null;
		}

		$idata = json_decode(file_get_contents(__DIR__ . '/../Data/jobs.json'), true);
		foreach ($data as $datum){
			if(!in_array($datum, $idata)){
				array_push($idata, $datum);
			}
		}
		file_put_contents(__DIR__.'/../Data/jobs.json', json_encode($idata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
		return null;
	}

	public function saveCompletedReports(array $data)
	{
		if(!file_exists(__DIR__ . '/../Data/reports.json')){
			file_put_contents(__DIR__.'/../Data/reports.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
			return null;
		}

		$idata = json_decode(file_get_contents(__DIR__ . '/../Data/reports.json'), true);
		foreach ($data as $datum){
			if(!in_array($datum, $idata)){
				array_push($idata, $datum);
			}
		}
		file_put_contents(__DIR__.'/../Data/reports.json', json_encode($idata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
		return null;
	}

	public function splitString(string $str, int $count = 4089)
	{
		$s = str_split($str, $count);
		$len = count($s);
		foreach ($s as $k => &$str){
			if($len != 1){
				if($k == 0){
					$str = $str.'...';
				}else if ($k == $len - 1){
					$str = '...'.$str;
				}else {
					$str = '...' . $str . '...';
				}
			}
		}
//		if($this->getDebug() > 2){echo print_r($s, true),PHP_EOL;};
		return $s;
	}

	public function speedLimit()
	{
		if(!isset($this->maxItemsJobsAllowedToExecute)){
			throw new \Exception('maxItemsJobsAllowedToExecute is not set');
		}
		if($this->maxItemsJobsAllowedToExecute <= 0){
			throw new \Exception('maxItemsJobsAllowedToExecute set to small');
		}
		if(!isset($this->minutesBetweenBlocks)){
			throw new \Exception('minutesBetweenBlocks is not set');
		}
		if($this->minutesBetweenBlocks <= 0){
			throw new \Exception('minutesBetweenBlocks set to small');
		}
		$this->speedLimitCounter++;
		if($this->speedLimitCounter >= $this->maxItemsJobsAllowedToExecute){
			echo "Preventing a speeding ticket.",PHP_EOL;
			echo "Closing cURL connection.",PHP_EOL;
			$this->curlClose();
			for ($i = $this->minutesBetweenBlocks; $i > 0; $i--){
				echo "Sleeping... $i...";
				sleep(30);
				echo " ZZZZ!",PHP_EOL;
				sleep(30);
			}
			echo "Green light!",PHP_EOL;
			echo "Initializing cURL connection.",PHP_EOL;
			$this->curlInit();
			$this->speedLimitCounter = 1;
		}
		return null;
	}

	public function getApiKey()
	{
		return $this->apiKey;
	}

	public function setApiKey($apiKey)
	{
		$this->apiKey = $apiKey;
	}

	public function getBaseUrl()
	{
		return $this->baseUrl;
	}

	public function setBaseUrl($baseUrl)
	{
		$this->baseUrl = $baseUrl;
	}

	public function getDebug()
	{
		return $this->debug;
	}

	public function setDebug($debug)
	{
		$this->debug = $debug;
	}

	public function isShowCustomFields(): bool
	{
		return $this->showCustomFields;
	}

	public function setShowCustomFields(bool $showCustomFields)
	{
		$this->showCustomFields = $showCustomFields;
	}

	public function getPageSize(): int
	{
		return $this->pageSize;
	}

	public function setPageSize(int $pageSize)
	{
		$this->pageSize = $pageSize;
	}

	public function getDefaultUserEmail()
	{
		return $this->defaultUserEmail;
	}

	public function setDefaultUserEmail($defaultUserEmail)
	{
		$this->defaultUserEmail = $defaultUserEmail;
	}

	public function isFirstRun(): bool
	{
		return $this->firstRun;
	}

	public function setFirstRun(bool $firstRun)
	{
		$this->firstRun = $firstRun;
	}

	public function getJobsFileSizeMax()
	{
		return $this->jobsFileSizeMax;
	}

	public function setJobsFileSizeMax($jobsFileSizeMax)
	{
		$this->jobsFileSizeMax = $jobsFileSizeMax;
	}

	public function getDefaultTaskUUID()
	{
		return $this->defaultTaskUUID;
	}

	public function setDefaultTaskUUID($defaultTaskUUID)
	{
		$this->defaultTaskUUID = $defaultTaskUUID;
	}

	public function getDefaultItemUUID()
	{
		return $this->defaultItemUUID;
	}

	public function setDefaultItemUUID($defaultItemUUID)
	{
		$this->defaultItemUUID = $defaultItemUUID;
	}

	public function getPartTrackingNumberStart(): int
	{
		return $this->partTrackingNumberStart;
	}

	public function setPartTrackingNumberStart(int $partTrackingNumberStart)
	{
		$this->partTrackingNumberStart = $partTrackingNumberStart;
	}

	public function getPartTrackingNumberPrefix(): string
	{
		return $this->partTrackingNumberPrefix;
	}

	public function setPartTrackingNumberPrefix(string $partTrackingNumberPrefix)
	{
		$this->partTrackingNumberPrefix = $partTrackingNumberPrefix;
	}

	public function getDefaultPartsTaskUUID(): string
	{
		return $this->defaultPartsTaskUUID;
	}

	public function setDefaultPartsTaskUUID(string $defaultPartsTaskUUID)
	{
		$this->defaultPartsTaskUUID = $defaultPartsTaskUUID;
	}

	public function getDefaultPmQ1UUID()
	{
		return $this->defaultPmQ1UUID;
	}

	public function setDefaultPmQ1UUID($defaultPmQ1UUID)
	{
		$this->defaultPmQ1UUID = $defaultPmQ1UUID;
	}

	public function getDefaultPmQ2UUID()
	{
		return $this->defaultPmQ2UUID;
	}

	public function setDefaultPmQ2UUID($defaultPmQ2UUID)
	{
		$this->defaultPmQ2UUID = $defaultPmQ2UUID;
	}

	public function getDefaultPmQ3UUID()
	{
		return $this->defaultPmQ3UUID;
	}

	public function setDefaultPmQ3UUID($defaultPmQ3UUID)
	{
		$this->defaultPmQ3UUID = $defaultPmQ3UUID;
	}

	public function getDefaultPmQ4UUID()
	{
		return $this->defaultPmQ4UUID;
	}

	public function setDefaultPmQ4UUID($defaultPmQ4UUID)
	{
		$this->defaultPmQ4UUID = $defaultPmQ4UUID;
	}

	public function getJobsToExecute()
	{
		return $this->jobsToExecute;
	}

	public function setJobsToExecute($jobsToExecute)
	{
		$this->jobsToExecute = $jobsToExecute;
	}

	public function getItemsToExecute()
	{
		return $this->itemsToExecute;
	}

	public function setItemsToExecute($itemsToExecute)
	{
		$this->itemsToExecute = $itemsToExecute;
	}

	public function getMinutesBetweenBlocks()
	{
		return $this->minutesBetweenBlocks;
	}

	public function setMinutesBetweenBlocks($minutesBetweenBlocks)
	{
		$this->minutesBetweenBlocks = $minutesBetweenBlocks;
	}

	public function getMaxItemsJobsAllowedToExecute()
	{
		return $this->maxItemsJobsAllowedToExecute;
	}

	public function setMaxItemsJobsAllowedToExecute($maxItemsJobsAllowedToExecute)
	{
		$this->maxItemsJobsAllowedToExecute = $maxItemsJobsAllowedToExecute;
	}





}