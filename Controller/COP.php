<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 9:48 PM
 */

namespace Controller;

use GetOptionKit\OptionPrinter\ConsoleOptionPrinter;
use GetOptionKit\OptionCollection;
use GetOptionKit\Option;

class COP extends ConsoleOptionPrinter
{
	public $screenWidth = 78;

	/**
	 * Render readable spec.
	 */
	public function renderOption(Option $opt)
	{
		$c1 = '';
		if ($opt->short && $opt->long) {
			$c1 = sprintf('-%s, --%s', $opt->short, $opt->long);
		} else if ($opt->short) {
			$c1 = sprintf('-%s', $opt->short);
		} else if ($opt->long) {
			$c1 = sprintf('--%s', $opt->long);
		}
		$c1 .= $opt->renderValueHint();

		return $c1;
	}

	/**
	 * render option descriptions.
	 *
	 * @return string output
	 */
	public function render(OptionCollection $options)
	{
		echo PHP_EOL,"Usage: run.php [-j|-i] <mode> <options>",PHP_EOL;
		echo         "Usage: run.php -c <options>",PHP_EOL;
		echo "A mode is required when the j or i flag is used.";
		echo "Modes will run in the order Download, Process, Execute.",PHP_EOL;
		echo "Suggested on windows to run with powershell rather than cmd!",PHP_EOL,PHP_EOL;
		$lines = array();
		foreach ($options as $option) {
			$c1 = $this->renderOption($option);
			$lines[] = "\t".$c1;
			$lines[] = wordwrap("\t\t".$option->desc, $this->screenWidth, "\n\t\t");  # wrap text
//			$lines[] = '';
		}

		return implode("\n", $lines);
	}
}