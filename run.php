<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 2:38 PM
 */
error_reporting(E_ALL & ~E_NOTICE);
set_time_limit(0);

require_once __DIR__.'/autoload.php';

use Controller\COP;
use Controller\FieldAwareApi;
use Controller\Colors;
use GetOptionKit\Exception\InvalidOptionException;
use GetOptionKit\OptionCollection;
use GetOptionKit\OptionParser;
use Repository\Item;
use Repository\ItemNew;
use Repository\Job;
use Repository\Labor;
use Repository\Task;
use Moment\Moment;
use Carbon\Carbon;
use ProgressBar\Manager;


define('INFO', 'cyan');
define('MSG', 'brown');
define('ERROR', 'red');
define('FILTER_FLAGS', FILTER_FLAG_ENCODE_HIGH);

$specs = new OptionCollection;
$specs->add('s|settings-make', 'Make new settings.json file. This will overwrite the existing one.' );
$specs->add('j|jobs', 'Process or Execute jobs');
$specs->add('i|items', 'Process or Execute items');
$specs->add('d|download-mode', 'Download all of the FieldAware api tables. (Asset, Contact, Customer, Item, Job, Location, Task, User)' );
$specs->add('p|process-mode+', 'Get all the jobs from the database that will be added to FieldAware and have an asset in the asset.json file. Jobs with errors will be saved in error_process_jobs.json file. Optional: Query limit and offset. The first value is the limit and the next is the offset. e.g. -p 50 -p 0' )
	->isa('number');
$specs->add('x|execute-mode?', 'Create the new FieldAware jobs contained in jobs.json file. Optional value to limit the number of jobs to create e.g. -x 10' )
	->isa('number');
$specs->add('c|custom-fields', 'Show custom fields and create custom_fields.txt then exit.');
$specs->add('q|delete-mode', "THis will delete all jobs from FieldAware that were gathered with -d");
$specs->add('v|verbose', 'Verbose messages. -v|-vv|-vvv' )
	->isa('Number')
	->incremental();
$specs->add('h|help', 'Print this help message.' );



$parser = new OptionParser($specs);
$c = new Colors();

try {
	$result = $parser->parse( $argv );
//	echo print_r($result->keys, true);

	if(key_exists('help', $result->keys) or count($result->keys) == 0){
		$printer = new COP();
		echo $printer->render($specs),PHP_EOL,PHP_EOL;
		die();
	}

	if(isset($result->keys['jobs']) and isset($result->keys['itmes'])){
		throw new InvalidOptionException('Can not use flags -j and -i at the same time!');
	}

	if(isset($result->keys['process-mode']) and count($result->keys['process-mode']->value) > 2){
		throw new InvalidOptionException('Process mode can only have 2 values. First is mysql limit, second is mysql offset.');
	}

} catch( Exception $e ) {
	echo $e->getMessage();
	die();
}
if(isset($result->keys['settings-make'])) {
	makeSettingsJson();
	exit(1);
}

if(!file_exists(__DIR__ . '/settings.json') or isset($result->keys['settings-mode'])){
	makeSettingsJson();
	exit(1);
}

$debug = false;
if(isset($result->keys['verbose'])){
	$debug = true;
}


$settings = loadSettingsJson($debug);

$api = new FieldAwareApi($settings['apiKey'], $settings['apiUrl']);
$api->setPageSize($settings['apiPageSize']);
$api->setDebug($result->verbose);
$api->setDefaultUserEmail($settings['defaultUserEmail']);
$api->setJobsFileSizeMax($settings['jobsFileSizeMax']);
$api->setDefaultTaskUUID($settings['defaultTaskUUID']);
$api->setDefaultPartsTaskUUID($settings['defaultPartsTaskUUID']);
$api->setDefaultItemUUID($settings['defaultItemUUID']);
$api->setPartTrackingNumberPrefix($settings['partTrackingNumberPrefix']);
$api->setPartTrackingNumberStart($settings['partTrackingNumberStart']);
$api->setMaxItemsJobsAllowedToExecute($settings['maxItemsJobsAllowedToExecute']);
$api->setjobsToExecute($settings['jobsToExecute']);
$api->setitemsToExecute($settings['itemsToExecute']);
$api->setMinutesBetweenBlocks($settings['minutesBetweenBlocks']);
$api->setDefaultPmQ1UUID($settings['defaultPmQ1UUID']);
$api->setDefaultPmQ2UUID($settings['defaultPmQ2UUID']);
$api->setDefaultPmQ3UUID($settings['defaultPmQ3UUID']);
$api->setDefaultPmQ4UUID($settings['defaultPmQ4UUID']);


if(isset($result->keys['custom-fields'])){
	$api->setShowCustomFields(true);
	$api->loadCustomFields();
	exit(0);
}

if(isset($result->keys['download-mode'])){
	echo $c->getColoredString("Download Begin: ".date(DATE_ISO8601).PHP_EOL, MSG);
	$api->getData('user/');
	$api->getData('location/');
	$api->getData('contact/');
	$api->getData('task/');
	$api->getData('customer/');
	$api->getData('item/');
	$api->getData('asset/');
	$api->getData('job/', '?start=2000-01-01T00%3A00&end=2069-12-30T00%3A00');
	$api->getData('settings/customfields/');

//	$api->getDataDetailAll('location');
	$api->getDataDetailAll('item');
	$api->getDataDetailAll('asset');
	$api->getDataDetailAll('task');
	echo $c->getColoredString("Downloading Complete", INFO),PHP_EOL;
	echo $c->getColoredString("Download End: ".date(DATE_ISO8601).PHP_EOL, MSG);
}

if(isset($result->keys['delete-mode'])){
	echo $c->getColoredString("Deleting Begin: ".date(DATE_ISO8601).PHP_EOL, MSG);
	$api->deleteJobs($api->loadData('job.json'));
	echo $c->getColoredString("Deleting Complete", INFO),PHP_EOL;
	echo $c->getColoredString("Download End: ".date(DATE_ISO8601).PHP_EOL, MSG);
	echo $c->getColoredString("Program End", INFO),PHP_EOL;
	exit(0);
}

if(isset($result->keys['process-mode'])){
	$mysqlLimit = false;
	$mysqlOffset = 0;

	$processBegin = new Carbon();
	echo $c->getColoredString("Process Begin: ".date(DATE_ISO8601).PHP_EOL, MSG);
	if(isset($result->keys['process-mode']->value) and count($result->keys['process-mode']->value) > 0 ){
		$mysqlLimit = $result->keys['process-mode']->value[0];
		if(isset($result->keys['process-mode']->value[1])){
			$mysqlOffset = $result->keys['process-mode']->value[1];
		}
	}
	if(isset($result->keys['jobs'])){
		processJobs($mysqlLimit, $mysqlOffset);
	}elseif (isset($result->keys['items'])){
		processItems($mysqlLimit, $mysqlOffset);
	}else{
		throw new \Exception('Must select jobs or items flags');
	}
	echo $c->getColoredString("Process End: ".date(DATE_ISO8601).PHP_EOL, MSG);

	$et = $processBegin->diff(new Carbon());
	echo $c->getColoredString(sprintf("Process Elapsed Time: %s days, %s:%s:%s", $et->d, $et->h, $et->i, $et->s).PHP_EOL, MSG);
}

if(isset($result->keys['execute-mode'])){
	$executeBegin = new Carbon();
	echo $c->getColoredString("Execute Begin: ".date(DATE_ISO8601).PHP_EOL, MSG);
	if(isset($result->keys['jobs'])){
		executeJobs($api->getJobsToExecute());
	}elseif (isset($result->keys['items'])){
		executeItems($api->getItemsToExecute());
	}else{
		throw new \Exception('Must select jobs or items flags');
	}
	echo $c->getColoredString("Execute End: ".date(DATE_ISO8601).PHP_EOL, MSG);
	$et = $executeBegin->diff(new Carbon());
	echo $c->getColoredString(sprintf("Process Elapsed Time: %s days, %s:%s:%s", $et->d, $et->h, $et->i, $et->s).PHP_EOL, MSG);
}

echo $c->getColoredString("Program End", INFO),PHP_EOL;
exit(0);

/**
 * Function below here
 */

/**
 * This function retrieves the data from the DB for jobs.
 * Modify the DB calls to fit your system.
 * We can only create jobs for assets and customers in FA
 * @param int $_limit
 * @param int $_offset
 * @throws \Exception
 * @internal param FieldAwareApi $c
 */
function processJobs($_limit, $_offset)
{
	global $settings, $api, $c;

	$customFields = $api->loadCustomFields(true);

//	$assetsData = uniqueArray(array_filter($api->loadData('asset'),"assetFilter" ), 'name');
	$assetsData = array_filter($api->loadData('asset'),"assetFilter" );
	echo $c->getColoredString("Loaded asset.json".PHP_EOL,INFO) ;

	$itemsData = $api->loadData('item');
	echo $c->getColoredString("Loaded item.json".PHP_EOL,INFO) ;

	$tasksData = $api->loadData('task');
	echo $c->getColoredString("Loaded task.json".PHP_EOL,INFO) ;

	$usersData = $api->loadData('user');
	echo $c->getColoredString("Loaded user.json".PHP_EOL,INFO) ;
	foreach ($usersData as $ukey => $usersDatum){
		$usersData[$ukey]['email'] = strtolower($usersDatum['email']);
	}

	$itemsDataDetail = $api->loadData('item_detail');
	echo $c->getColoredString("Loaded item_detail.json".PHP_EOL,INFO) ;

	$assetDataDetail = $api->loadData('asset_detail');
	echo $c->getColoredString("Loaded asset_detail.json".PHP_EOL,INFO) ;


	$limit = '';
	if($_limit !== false){
		$limit = ' LIMIT '.$_offset.", ".$_limit;
	}
	echo $c->getColoredString("Query using LIMIT ".$_offset.", ".$_limit.PHP_EOL,INFO) ;
	$mysqli = new mysqli($settings['mysqlHost'], $settings['mysqlUsername'], $settings['mysqlPassword'], $settings['mysqlDb'], $settings['mysqlPort']);
	if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

	$tmp = array_search($api->getDefaultTaskUUID(), array_column($tasksData, 'uuid'));
	if ($tmp !== false) {
		$portalMiscUUID = $tasksData[$tmp]['uuid'];
	}else{
		throw new \Exception('Could not find T99.00 Portal Misc. task!');
	}

	$tmp = array_search($api->getDefaultPartsTaskUUID(), array_column($tasksData, 'uuid'));
	if ($tmp !== false) {
		$portalPartsUUID = $tasksData[$tmp]['uuid'];
	}else{
		throw new \Exception('Could not find T99.10 Portal Parts task!');
	}

	$tmp = array_search($api->getDefaultItemUUID(), array_column($itemsData, 'uuid'));
	if ($tmp !== false) {
		$portalItemUUID = $itemsData[$tmp]['uuid'];
	}else{
		throw new \Exception('Could not find T99.10 Portal Item item!');
	}

	$completedAssets = [];
	$completedJobs = [];
	$errorJobs = [];
	$assetLimit = 1;
	$assetCount = count($assetsData) + 1;
	$x = 1;
	foreach ($assetsData as $asset){
		echo $c->getColoredString("Processing Asset: ".$asset['name']."  ($x of $assetCount)".PHP_EOL,INFO) ;
		echo $c->getColoredString("\tGetting Details".PHP_EOL,INFO) ;

		$assetDetail = [];
		$tmp = array_search($asset['name'], array_column($assetDataDetail, 'name'));
		if($tmp !== false) {
			$assetDetail = $assetDataDetail[$tmp];
		}else{
			echo "TMP: ",print_r($tmp),PHP_EOL;
			throw new \Exception('Could not find asset: '.$asset['name']);
		}

		$assetName = explode(' ', $asset['name'])[0];

		$sql = "SELECT sr.report_id, sr.system_unique_id, sr.system_ver_unique_id, sr.facility_unique_id, sr.system_id,
				sr.system_nickname, 
				sr.complaint, sr.service, sr.engineer, pe.email as eng_email,
				st.`status`,
				up.email AS eng_pri, us.email AS eng_sec, up.name AS eng_pri_name, us.name AS eng_sec_name,
				sr.finalize_date, sr.po_number, sr.contract_override, srq.initial_call_date, srq.onsite_date, srq.response_date, srq.response_minutes,
				sr.slice_mas_count, sr.gantry_rev, sr.sys_scn_secs, sr.tube_scn_secs, sr.helium_level, sr.vessel_pressure, sr.recon_ruo, sr.cold_ruo, sr.compressor_hours, sr.compressor_pressure, 
				sr.downtime, sr.pm, sr.pm_date, sr.refrig_service, sr.phone_fix, 
				sr.unique_id, sbc.contact_email,
				f.name, f.address, f.city, f.state, f.zip,
				sr.refrig_service, sr.invoice_required, sr.valuation_complete, sr.valuation_uid, sr.valuation_date, sr.valuation_do_not_invoice, 
				sr.invoice_labor_reg_rate, sr.invoice_labor_ot_rate, sr.invoice_travel_reg_rate, sr.invoice_travel_ot_rate, sr.invoice_labor_reg, 
				sr.invoice_labor_ot, sr.invoice_travel_reg, sr.invoice_travel_ot, sr.invoice_shipping, sr.invoice_expense, sr.invoiced, sr.invoice_number, mc.invoiced AS non_pl
				-- ,
				-- (SELECT count(*)
				-- FROM systems_parts
				-- WHERE unique_id = sr.unique_id) as parts
				
				FROM systems_reports AS sr
				LEFT JOIN systems_requests AS srq ON srq.unique_id = sr.unique_id
				LEFT JOIN systems_status AS st ON st.id = sr.system_status
				LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sr.system_ver_unique_id
				LEFT JOIN systems_assigned_sec AS sas ON sas.system_ver_unique_id = sr.system_ver_unique_id
				LEFT JOIN users AS up ON up.uid = sap.uid
				LEFT JOIN users AS us ON us.uid = sas.uid
				LEFT JOIN users AS pe ON pe.uid = sr.engineer
				LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sr.system_ver_unique_id
				LEFT JOIN facilities AS f ON f.unique_id = sr.facility_unique_id
				LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
				WHERE sr.system_id LIKE ('%".$assetName."%') AND sr.fa_imported = 0
				ORDER BY sr.report_id DESC".$limit.";";
		echo $api->getDebug() ? $c->getColoredString("\tCollecting base data".PHP_EOL, MSG): '';
		if(!$result = $mysqli->query($sql)){
			throw new \Exception('Error when trying to get result for: '.$sql);
		}
		$data = [];
		while ($row = $result->fetch_assoc()){
			array_push($data, $row);
		}

		$dataCount = count($data);
		$z = 1;
		$pbar = new Manager(0, $dataCount);
		foreach ($data as $key => $val){
			if(!$api->getDebug()){
				$pbar->advance();
			}
			try {
				//Hours
				echo $api->getDebug() ? $c->getColoredString("\tCollecting hours" . PHP_EOL, MSG) : '';
				$sql = "SELECT id, `date`, reg_labor, ot_labor, reg_travel, ot_travel, phone
						FROM systems_hours
						WHERE unique_id = '" . $val['unique_id'] . "';";
				if (!$result = $mysqli->query($sql)) {
					throw new \Exception('Error when trying to get result for: ' . $sql);
				}
				while ($row = $result->fetch_assoc()) {
					$data[$key]['hours'][$row['id']] = $row;
				}

				//Parts
				echo $api->getDebug() ? $c->getColoredString("\tCollecting parts" . PHP_EOL, MSG) : '';
				$sql = "SELECT id, number, serial, description, qty, price
						FROM systems_parts
						WHERE unique_id = '" . $val['unique_id'] . "';";
				if (!$result = $mysqli->query($sql)) {
					throw new \Exception('Error when trying to get result for: ' . $sql);
				}
				while ($row = $result->fetch_assoc()) {
					$data[$key]['parts'][$row['id']] = $row;
				}

				//Journal
				echo $api->getDebug() ? $c->getColoredString("\tCollecting journal" . PHP_EOL, MSG) : '';
				$sql = "SELECT ssj.note, ssj.`date`, u.name
						FROM systems_service_journal as ssj
						LEFT JOIN users AS u ON u.uid = ssj.uid
						WHERE ssj.request_report_unique_id  = '".$val['unique_id']."';";
				if (!$result = $mysqli->query($sql)) {
					throw new \Exception('Error when trying to get result for: ' . $sql);
				}
				$journals = "";
				while ($row = $result->fetch_assoc()) {
					$journals .= $row['date']." ".$row['name'].' - '.$row['note']."\r\n";
				}


				$data[$key]['asset_uuid'] = $asset['uuid'];

				//Job
				echo $api->getDebug() ? $c->getColoredString("\tCreating job ". $val['report_id']." ($z of $dataCount)" . PHP_EOL, MSG) : '';
				$job = new Job();

				$job->setDescription(substr(trim(stringVal($val['complaint'])), 0, 4090));

				$job->setAssetUUID($asset['uuid']);

				if ($tmp = array_search(strtolower($val['eng_email']), array_column($usersData, 'email'))) {
					$job->setJobLeadUUID($usersData[$tmp]['uuid']);

				} elseif ($tmp = array_search(strtolower($api->getDefaultUserEmail()), array_column($usersData, 'email'))) {
					$job->setJobLeadUUID($usersData[$tmp]['uuid']);
				} else {
					throw new \Exception('Could not find the default users email in the users list! REPORT ID: '.$val['report_id']);
				}

				if(isset($assetDetail['contact']['uuid'])){
					$job->setContactUUID($assetDetail['contact']['uuid']);
				}

				$job->setScheduledOn(rtrim($val['onsite_date'], 'Z'));

				if(isset($assetDetail['location']['uuid'])){
					$job->setLocationUUID($assetDetail['location']['uuid']);
				}else{
					throw new \Exception("\tCould not find location for the job! REPORT ID: ".$val['report_id']);
				}

				//Custom Fields
				$jobCustomFields = [
					$settings['customFields']['Job']['Compressor Hours'] => floatValue($val['compressor_hours']),
					$settings['customFields']['Job']['Facility Unique ID'] => stringVal($val['facility_unique_id']),
					$settings['customFields']['Job']["Gantry Revolutions"] => floatValue($val['gantry_rev']),
					$settings['customFields']['Job']["Helium Level"] => floatValue($val['helium_level']),
					$settings['customFields']['Job']["Hours System was down"] => floatValue($val['downtime']),
					$settings['customFields']['Job']["Primary Engineer"] => stringVal($val['eng_pri_name']),
					$settings['customFields']['Job']["Purchase Order Number"] => stringVal($val['po_number']),
					$settings['customFields']['Job']["Report Unique ID"] => $val['unique_id'],
					$settings['customFields']['Job']["ReportID"] => stringVal($val['report_id']),
					$settings['customFields']['Job']["Secondary Engineer"] => stringVal($val['eng_sec_name']),
					$settings['customFields']['Job']["Job Status"] => "Closed", //Status,
					$settings['customFields']['Job']["System Unique ID"] => $val['system_ver_unique_id'],
					$settings['customFields']['Job']["System ID"] => stringVal($val['system_id']),
					$settings['customFields']['Job']["Vessel Pressure"] => floatValue($val['vessel_pressure']),
					$settings['customFields']['Job']["MAs/Scan Seconds"] => floatValue($val['slice_mas_count']),
					$settings['customFields']['Job']["Phone Fix"] => yn($val['phone_fix']),
					$settings['customFields']['Job']["Initial Call Date"] => dt($val['initial_call_date']),
					$settings['customFields']['Job']["Initial Call Time"] => tm($val['initial_call_date']),
					$settings['customFields']['Job']["Response Date"] => dt($val['response_date']),
					$settings['customFields']['Job']["Response Time"] => tm($val['response_date']),
					$settings['customFields']['Job']["Response Minutes"] => floatValue($val['response_minutes']),
					$settings['customFields']['Job']["System Status"] => stringVal($val['status']),
				];

				if(yn($val['pm'])){
					$jobCustomFields[$settings['customFields']['Job']['Job Type']] = "System PM";
				}else{
					$jobCustomFields[$settings['customFields']['Job']['Job Type']] = "System Issue";
				}

				if($journals != ""){
					$jobCustomFields[$settings['customFields']['Job']['Comments History']] = stringVal(substr($journals,0, 4095)); //Comments History
				}

				$job->setCustomFields($jobCustomFields);


				//Labor
				$pl = true;
				if(yn($val['non_pl'])){
					$pl = false;
				}
				$invoiced = false;
				if(yn($val['invoiced'])){
					$invoiced = true;
				}
				$hours = "Hours From Portal:\r\n";
				foreach ($data[$key]['hours'] as $day){
					$hours .= vsprintf("[%d] %s RL: %g OL: %g RT: %g OT: %g Ph: %g\r\n", $day);
					if(floatValue($day['reg_labor']) > 0){
						$labor = new Labor();
						$labor->setUserUUID($job->getJobLeadUUID());
						$labor->setQuantity(floatValue($day['reg_labor'])*60);
						if($invoiced){
							$rate = false;
							if($val['invoice_labor_reg_rate'] != ""){
								$rate = floatValue($val['invoice_labor_reg_rate']);
							}
							$labor->setRate('sbrRegular', $rate);
						}else{
							if($pl){
								$labor->setRate('contractLabor');
							}else{
								$labor->setRate('sbrRegular');
							}
						}
						$job->addLabor($labor);
						unset($labor);
					}
					if(floatValue($day['ot_labor']) > 0){
						$labor = new Labor();
						$labor->setUserUUID($job->getJobLeadUUID());
						$labor->setQuantity(floatValue($day['ot_labor'])*60);
						if($invoiced){
							$rate = false;
							if($val['invoice_labor_ot_rate'] != ""){
								$rate = floatValue($val['invoice_labor_ot_rate']);
							}
							$labor->setRate('sbrOvertime', $rate);
						}else{
							if($pl){
								$labor->setRate('plOvertime');
							}else{
								$labor->setRate('sbrOvertime');
							}
						}
						$job->addLabor($labor);
						unset($labor);
					}
					if(floatValue($day['reg_travel']) > 0){
						$labor = new Labor();
						$labor->setUserUUID($job->getJobLeadUUID());
						$labor->setQuantity(floatValue($day['reg_travel'])*60);
						if($invoiced){
							$rate = false;
							if($val['invoice_travel_reg_rate'] != ""){
								$rate = floatValue($val['invoice_travel_reg_rate']);
							}
							$labor->setRate('sbrTravel', $rate);
						}else{
							if($pl){
								$labor->setRate('contractTravel');
							}else{
								$labor->setRate('sbrTravel');
							}
						}
						$job->addLabor($labor);
						unset($labor);
					}
					if(floatValue($day['ot_travel']) > 0){
						$labor = new Labor();
						$labor->setUserUUID($job->getJobLeadUUID());
						$labor->setQuantity(floatValue($day['ot_travel'])*60);
						if($invoiced){
							$rate = false;
							if($val['invoice_travel_ot_rate'] != ""){
								$rate = floatValue($val['invoice_travel_ot_rate']);
							}
							$labor->setRate('sbrTravel', $rate);
						}else{
							if($pl){
								$labor->setRate('contractTravel');
							}else{
								$labor->setRate('sbrTravel');
							}
						}
						$job->addLabor($labor);
						unset($labor);
					}
					if(floatValue($day['phone']) > 0){
						$labor = new Labor();
						$labor->setUserUUID($job->getJobLeadUUID());
						$labor->setQuantity(floatValue($day['phone'])*60);
						if($invoiced){
							$labor->setRate('techSupport');
						}else{
							if($pl){
								$labor->setRate('contractTech');
							}else{
								$labor->setRate('techSupport');
							}
						}
						$job->addLabor($labor);
						unset($labor);
					}
				}


				if ($tmp = array_search('Serial Number', array_column($customFields, 'name'))) {
					$serialUUID = $customFields[$tmp]['uuid'];
				}else{
					throw new \Exception('Could not find Serial Number custom field in settings_customFields.json!');
				}

				//Hours Task
				$task = new Task();
				$task->setTaskUUID($portalMiscUUID);
				$task->setAssetUUID($job->getAssetUUID());
				$task->setAssigneeUUID($job->getJobLeadUUID());
				$task->setNote($hours);
				$task->setDone(true);
				$job->addTask($task);
				unset($task);

				//Parts Task
				$partsDesc = "Parts from portal:\r\n";
				if(count($data[$key]['parts']) > 0){
					if($api->getDebug() > 2){echo "DATA[KEY][PARTS]: ",print_r($data[$key]['parts'], true),PHP_EOL;}
					if($api->getDebug() > 1){echo $c->getColoredString("\tAdding parts", MSG),PHP_EOL;}
					$task = new Task();
					$task->setTaskUUID($portalPartsUUID);
					$task->setAssetUUID($job->getAssetUUID());
					$task->setAssigneeUUID($job->getJobLeadUUID());
					$task->setDone(true);

					foreach ($data[$key]['parts'] as $part){
						$partsDesc .= vsprintf("[%s] PN: %s SN: %s DESC: %s QTY: %s PRICE: %s\r\n", $part);
						if($api->getDebug() > 1){echo $c->getColoredString("\t\tPart: ".$part['description'], MSG),PHP_EOL;}
						$uuid = $portalItemUUID;
						foreach ($itemsDataDetail as $idd){
//							if($api->getDebug() > 2){echo "IID: ",print_r($idd, true),PHP_EOL;}
							if(isset($idd['customFields'][$settings['customFields']['Items']['Serial Number']])
								and $idd['customFields'][$settings['customFields']['Items']['Serial Number']] == $part['serial']
								and $idd['partNumber'] == $part['number']){
								$uuid = $idd['uuid'];
								if($api->getDebug() > 1){echo $c->getColoredString("\t\t\tFound in items: serial: ".$part['serial'], MSG),PHP_EOL;}
								if($api->getDebug() > 2) {
									echo "PART['SERIAL']: ", $part['serial'], PHP_EOL;
									echo "UUID: ", $uuid, PHP_EOL;
									echo "CUSTOMFIELDS: ", print_r($idd['customFields'], true), PHP_EOL;
								}
								break;
							}
						}


						$item = new Item();
						$item->setUuid($uuid);
						$item->setQuantity(intval($part['qty']));
						if(floatValue($part['price']) != 0 ){
							$item->setUnitPrice(floatValue($part['price']));
						}

						$customFieldsItem = [
							$settings['customFields']['Items']['Serial Number'] => stringVal($part['serial']),
							$settings['customFields']['Items']['Product Family'] => "Part", //Product Family
							$settings['customFields']['Items']['Product Status'] =>  "Installed", //Product Status
						];
						$item->setCustomField($customFieldsItem);
//						echo print_r($item,true);die();
						$task->addItem($item);
					}
					$task->setNote(stringVal($partsDesc));
					$job->addTask($task);
				}

				//Service
				$service = $api->splitString(stringVal(trim($val['service'])));
				foreach ($service as $sText){
					$task = new Task();
					$task->setTaskUUID($portalMiscUUID);
					$task->setAssetUUID($job->getAssetUUID());
					$task->setAssigneeUUID($job->getJobLeadUUID());
					$task->setDone(true);
					$task->setNote($sText);
					$job->addTask($task);
					unset($task);
				}

				$portalInfo = sprintf(
					"Eng Assigned: %s;\r\nInitial Call DateTime: %s; Response DateTime: %s; Onsite Date: %s;\r\nPM: %s; PM Date: %s;\r\nInvoiced: %s; Invoice No: %s;",
					$val['engineer'],
					$val['initial_call_date'],
					$val['response_date'],
					$val['onsite_date'],
					$val['pm'],
					$val['pm_date'],
					$val['invoiced'],
					$val['invoice_number']
					);

				$task = new Task();
				$task->setTaskUUID($portalMiscUUID);
				$task->setAssetUUID($job->getAssetUUID());
				$task->setAssigneeUUID($job->getJobLeadUUID());
				$task->setDone(true);
				$task->setNote($portalInfo);
				$job->addTask($task);
				unset($task);

				//PM Task
				if(yn($val['pm'])) {
					$pmdate = Carbon::parse($val['pm_date']);
					$qtr = $pmdate->quarter;
					$taskUUID = "";
					switch ($qtr) {
						case 1:
							$taskUUID = $api->getDefaultPmQ1UUID();
							break;
						case 2:
							$taskUUID = $api->getDefaultPmQ2UUID();
							break;
						case 3:
							$taskUUID = $api->getDefaultPmQ3UUID();
							break;
						case 4:
							$taskUUID = $api->getDefaultPmQ4UUID();
							break;
					}
					$task = new Task();
					$task->setTaskUUID($taskUUID);
					$task->setAssetUUID($job->getAssetUUID());
					$task->setAssigneeUUID($job->getJobLeadUUID());
					$task->setDone(true);
					$task->setNote(sprintf("PM: %s; PM Date: %s; PM Quarter: %s;",$val['pm'], $val['pm_date'], $qtr));
					$customFieldsTask = [
						$settings['customFields']['Task']['PM Task'] => true,
						$settings['customFields']['Task']['PM Completed Date'] => dt($val['pm_date'])
					];
					$task->setCustomFields($customFieldsTask);
					$job->addTask($task);
					unset($task);
				}

				$preSaveJson = json_encode($job, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
				if($api->getDebug() > 2){echo "PRE-SAVE JOB: ",$preSaveJson,PHP_EOL;}
				if(strlen($preSaveJson) > 50){
					if(count($job->getTasks()) < 2){
						throw new \Exception('Job does not contain enough tasks! REPORT ID: '.$val['report_id']);
					}
					$api->saveJobData($job);
				}else{
					unset($job);
					throw new \Exception('Pre-save job check failed. Job data was blank. REPORT ID: '.$val['report_id']);
				}


			}catch (\Exception $e){

				echo $c->getColoredString($e->getMessage(), ERROR),PHP_EOL;

				if(!isset($job)){
					$job = null;
				}
				array_push($errorJobs, ["Error" => $e->getMessage(), "Data" => $data[$key], 'Job' => $job]);
			}
			$z++;
		}//end foreach report
		$assetLimit++;
		if($_limit > 0 and $assetLimit > $_limit){
			break;
		}
		$x++;
	}//end asset for each
	$mysqli->close();
	echo $c->getColoredString(count($errorJobs).' jobs found containing errors.'.PHP_EOL, ERROR);
	if($api->getDebug() > 1){
		echo $c->getColoredString('Jobs with errors:'.PHP_EOL, ERROR);
		echo $c->getColoredString(print_r($errorJobs, true), ERROR),PHP_EOL;

	}

	$api->saveData($errorJobs, 'error_process_jobs.json');

	echo $c->getColoredString("Processing Jobs Complete", INFO),PHP_EOL;
}

function processItems($_limit, $_offset)
{
	global $settings, $api, $c;

	$customFields = $api->loadCustomFields(true);

	$itemsData = $api->loadData('item');
	echo $c->getColoredString("Loaded item.json".PHP_EOL,INFO) ;

	$itemsDataDetail = $api->loadData('item_detail');
	echo $c->getColoredString("Loaded item_detail.json".PHP_EOL,INFO) ;

	$limit = '';
	if($_limit !== false){
		$limit = ' LIMIT '.$_offset.", ".$_limit;
	}
	echo $c->getColoredString("Query using LIMIT ".$_offset.", ".$_limit.PHP_EOL,INFO) ;
	$mysqli = new mysqli($settings['mysqlHost'], $settings['mysqlUsername'], $settings['mysqlPassword'], $settings['mysqlDb'], $settings['mysqlPort']);
	if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

	echo $api->getDebug() ? $c->getColoredString("\tCollecting base data".PHP_EOL, MSG): '';
	$sql = "SELECT p.id, p.report_id, p.number, p.`serial`, p.description, p.qty, p.price, p.unique_id, p.system_unique_id, p.system_ver_unique_id, p.system_ver, p.`date`, st.`type`
			FROM systems_parts AS p
			LEFT JOIN systems_base AS sb ON sb.unique_id = p.system_unique_id
			LEFT JOIN systems_types AS st ON st.id = sb.system_type
			WHERE p.`serial` NOT REGEXP '[0+|9+|x+|n/a]'
			AND p.`serial` NOT IN('notfnd', 'rcvr', 'none')
			AND p.`serial` != ''
			AND LENGTH(p.`serial`) > 3
			ORDER BY p.id".$limit.";";
	if(!$result = $mysqli->query($sql)){
		throw new \Exception('Error when trying to get result for: '.$sql);
	}
	$data = [];
	while ($row = $result->fetch_assoc()){
		array_push($data, $row);
	}
	$result->free();
	$mysqli->close();

	$trackingNumber = $api->loadTrackingNumberLast();
	if(!$trackingNumber){
		$trackingNumber = $api->getPartTrackingNumberStart();
	}else{
		$trackingNumber++;
	}


	foreach ($data as $part){
		if($api->getDebug() > 2){echo print_r($part, true),PHP_EOL;}
		$itemNew = new ItemNew();
		$itemNew->setName($api->getPartTrackingNumberPrefix().$trackingNumber.' '.substr($part['description'],0,32));
		$itemNew->setDescription($part['description']);
		$itemNew->setPartNumber($part['number']);
		$itemNew->setUnitPrice(floatValue($part['price']));
		$customFields = [
			$settings['customFields']['Items']['Product Family'] => "Part", //Product Family
			$settings['customFields']['Items']['Product Status'] => "Installed",
			$settings['customFields']['Items']['Product Type'] => "CT", //Product Type
			$settings['customFields']['Items']['Serial Number'] => $part['serial'],
		];
		if(strtolower($part['type']) == 'ct'){
			$customFields[$settings['customFields']['Items']['Product Type']] = "CT";
		}else{
			$customFields[$settings['customFields']['Items']['Product Type']] = "MRI";
		}
		$itemNew->setCustomField($customFields);
		if($api->getDebug() > 2){echo print_r($itemNew, true);}


		$api->saveItemData($itemNew);
		$api->saveTrackingNumberLast($trackingNumber);
		$trackingNumber++;

	}
	echo $c->getColoredString("Processing Items Complete", INFO),PHP_EOL;
}

function executeJobs($max)
{
	global $settings, $api, $c;

	$errorJobs = [];
	$executed = 1;
	$completedJobs = [];
	$completedReports = $api->loadData('reports.json');

	$mysqli = new mysqli($settings['mysqlHost'], $settings['mysqlUsername'], $settings['mysqlPassword'], $settings['mysqlDb'], $settings['mysqlPort']);
	if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

	$z = 1;
	$dataCount = $api->getJobsToExecute();
	foreach (glob(__DIR__.'/Data/Jobs/*.json') as $file) {
		$data = $api->loadJobData(basename($file));
		if ($api->getDebug() > 2){echo print_r($data, true), PHP_EOL;}
		foreach ($data as $key => $job) {
			if (isset($job['JobCreated'])) {
				if($api->getDebug() > 1) {
					echo $c->getColoredString("The job " . $job['customFields'][$settings['customFields']['Job']['ReportID']] . " has already been created in FieldAware... Skipping", ERROR), PHP_EOL;
				}
				array_push($errorJobs, $job);
				$matches = [];
				preg_match("/^J\d*/", $job['JobCreated'], $matches);
				if (!is_null($matches[0])) {
					array_push($completedJobs, $matches[0]);
				}
				$api->saveCompletedJobs($completedJobs);

				array_push($completedReports, $job['customFields'][$settings['customFields']['Job']['ReportID']]);
				$api->saveCompletedReports($completedReports);
				continue;
			}elseif (isset($job['customFields'][$settings['customFields']['Job']['ReportID']]) and in_array($job['customFields'][$settings['customFields']['Job']['ReportID']], $completedReports)){
				if($api->getDebug() > 1) {
					echo $c->getColoredString("The report " . $job['customFields'][$settings['customFields']['Job']['ReportID']] . " has already been created in FieldAware... Skipping", ERROR), PHP_EOL;
				}
				$data[$key]['JobCreated'] = $job['customFields'][$settings['customFields']['Job']['ReportID']];
				$api->saveJsonFile($file, $data);
				continue;
			} else {
				if ($api->getDebug() > 0){echo $c->getColoredString("Creating Job: " . $job['customFields'][$settings['customFields']['Job']['ReportID']]." ($z of $dataCount) ", MSG);}
			}

			$rtn = $api->putData($job, 'job/');
			if(is_string($rtn)){
				$data[$key]['JobCreated'] = $rtn;
				$matches = [];
				preg_match("/^J\d*/", $rtn, $matches);
				if (!is_null($matches[0])) {
					array_push($completedJobs, $matches[0]);
					if ($api->getDebug() > 0){echo $c->getColoredString($matches[0], MSG),PHP_EOL;}
				}
				$api->saveJsonFile($file, $data);
				$api->saveCompletedJobs($completedJobs);

				array_push($completedReports, $job['customFields'][$settings['customFields']['Job']['ReportID']]);
				$api->saveCompletedReports($completedReports);

				$sql = "UPDATE systems_reports SET fa_imported = 1 WHERE report_id = '".$job['customFields'][$settings['customFields']['Job']['ReportID']]."';";
				if (!$result = $mysqli->query($sql)) {
					throw new \Exception('Error when trying to get result for: ' . $sql);
				}
				$z++;

			}else{
				echo $c->getColoredString('There was an error in the PUT request!', ERROR),PHP_EOL;
				echo $c->getColoredString(print_r($rtn), ERROR),PHP_EOL;
				$job['ERROR'] = $rtn['RESPONSE'];
				array_push($errorJobs, $job);
			}

			$executed++;
			if($executed > $max and $max != -1){
				break;
			}
			$api->speedLimit();

		}//foreach job

		if($executed > $max and $max != -1){
			break;
		}
		$api->speedLimit();
	}//foreach file

	$api->saveData($errorJobs, 'error_execute_jobs.json');
	echo $c->getColoredString("Executing Jobs Complete", INFO),PHP_EOL;
	echo $c->getColoredString("Created/Updated jobs.json for use with FAComplete", INFO),PHP_EOL;
}

function executeItems($max)
{
	global $settings, $api, $c;

	$errorItems = [];
	$used = $api->loadTrackingNumberUsed();
	$executed = 1;

	foreach (glob(__DIR__.'/Data/Items/*.json') as $file){
		$data = $api->loadItemData(basename($file));
		if($api->getDebug() > 2){echo print_r($data, true),PHP_EOL;}
		foreach ($data as $key => $item){

			if(isset($item['ItemCreated'])){
				if($api->getDebug() > 1) {
					echo $c->getColoredString("The item " . $item['name'] . " has already been created in FieldAware... Skipping", ERROR), PHP_EOL;
				}
				array_push($errorItems, $item);
				continue;
			}else{
				if($api->getDebug() > 0){echo $c->getColoredString("Creating Item: ".$item['name'], MSG),PHP_EOL;}
			}
			$matches = [];
			$pattern = $api->getPartTrackingNumberPrefix().'\d{1,6} ';
			preg_match("/$pattern/", $item['name'], $matches);
			if(in_array(trim($matches[0]), $used)){
				echo $c->getColoredString('The tracking number '.$matches[0].' has already been used!', ERROR),PHP_EOL;
				array_push($errorItems, $item);
				continue;
			}
			$rtn = $api->putData($item, 'item/');
			if(is_string($rtn)){
				array_push($used,trim($matches[0]));
				$api->saveTrackingNumberUsed($used);
				$data[$key]['ItemCreated'] = trim($matches[0]);
				$api->saveJsonFile($file, $data);
			}else{
				echo $c->getColoredString('There was an error in the PUT request!', ERROR),PHP_EOL;
				echo $c->getColoredString(print_r($rtn), ERROR),PHP_EOL;
			}

			$executed++;
			if($executed > $max and $max != -1){
				break;
			}
		}
		if($executed > $max and $max != -1){
			break;
		}
	}
	$api->saveData($errorItems, 'error_execute_items.json');
	echo $c->getColoredString("Executing Items Complete", INFO),PHP_EOL;
}

function uniqueArray($arr, $col)
{
	$newArr = [];
	$cols = array_unique(array_column($arr, $col));
	foreach ($arr as $key=>$value){
		if(key_exists($key, $cols)){
			$newArr[$key] = $arr[$key];
		}
	}
	return $newArr;
}

function assetFilter($v)
{
	$excludes = array("CT1292","CT1293","CT1294","CT1295","CT1296","CT1297","CT1298","CT1299","CT1300","CT1301","CT1306","CT1311","CT1312","CT1323","MR2154","MR2202","MR2208","MR2222","MR2234","MR2246","MR2260","MR2261","MR2263","MR2265","MR2266","MR2267","MR2268","MR2269","MR2270","MR2271","MR2272","MR2273","MR2274","MR2275","MR2276","MR2277","MR2279","MR2280","MR2282","MR2283","MR2286","MR2289","MR2290","MR2332","MR2307","MR2308","MR2330","MR2339","MR2373","NM3000","NM3001","NM3002","PCT4000", "MR2259");
	$tmp = explode(' ', $v['name']);
	if(in_array($tmp[0], $excludes)){
		echo $tmp[0] . " Skipping because exists in excludes assetFilter",PHP_EOL;
		return false;
	}
	return true;
}

function loadSettingsJson($debug)
{
	$file = __DIR__ . '/settings.json';
	$fs = fopen($file, 'r') or die('Cannot open file settings.json');
	$json = fread($fs,filesize($file)+1);
	$json = json_decode($json, true);
	if($debug){
		echo "Settings.json: ",PHP_EOL,print_r($json),PHP_EOL;
	}
	foreach ($json as $key => $val){
		if($val == '' and $key != 'defaultLocationUUID'){
			throw new \Exception('File setting.json is invalid!');
		}
	}
	return $json;
}

function makeSettingsJson()
{
	$data = [
		"apiKey" => "",
        "apiPageSize" => 20,
        "apiUrl" => "https://api.fieldaware.net/",
		"mysqlHost" => "",
		"mysqlPort" => 3306,
		"mysqlDb" => "",
		"mysqlUsername" => "",
		"mysqlPassword" => "",
		"defaultUserEmail" => "",
		"defaultTaskUUID" => "",
		"defaultPartsTaskUUID" => "",
		"defaultItemUUID" => "",
		"defaultPmQ1UUID" => "",
	    "defaultPmQ2UUID" => "",
	    "defaultPmQ3UUID" => "",
	    "defaultPmQ4UUID" => "",
		"jobsFileSizeMax" => 2048000,
		"partTrackingNumberPrefix" => "",
		"partTrackingNumberStart" => 1000,
		"jobsToExecute" => -1,
		"itemsToExecute" => -1,
		"maxItemsJobsAllowedToExecute" => 200,
		"minutesBetweenBlocks" => 1,
		"customFields" => [
			"Item" => [],
			"Job" => [],
			"Task" => []
		],
	];
	$fs = fopen(__DIR__ . '/settings.json', 'wt') or die('Cannot open file settings.json');
	fwrite($fs, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
	fclose($fs);

	$c = new Colors();
	echo $c->getColoredString(
		'Created new settings.json file.'.PHP_EOL.'Edit the file and fill in the missing info.'.PHP_EOL.'Then re-run \''.basename(__FILE__).'\'.'.PHP_EOL.PHP_EOL,
		ERROR
	);

}

function yn($str)
{
	if(strtolower($str) == 'y'){
		return true;
	}
	return false;
}

function dt($str)
{
	$m = new Moment($str);
	return $m->format("Y-m-d");
}

function tm($str)
{
	$m = new Moment($str);
	return $m->format("H:i:s");
}

function getMinutes($_date_1, $_date_2)
{
	$date_1 = new Moment($_date_1);
	$date_2 = new Moment($_date_2);
	echo print_r($date_1->diff($date_2));
	die();
}

function floatValue($str)
{
	if(!is_string($str)){
		return 0;
	}
	$clean = preg_replace("/[^\d\.]+/","",$str);
	$f = floatval($clean);
	if(is_float($f)){
		return $f;
	}else{
		return 0;
	}
}

/**
 * @param mixed $str
 * @return mixed
 */
function stringVal($str)
{
	if(!is_string($str)){
		$str = '';
	}
	return filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAGS);
}