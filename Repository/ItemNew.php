<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 6:14 PM
 */

namespace Repository;

use Controller\JsonDeserializer;


/**
 * Class Item
 * @package Repository
 */
class ItemNew extends JsonDeserializer implements \JsonSerializable
{

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $partNumber;

	/**
	 * @var float
	 */
	public $unitCost;

	/**
	 * @var float
	 */
	public $unitPrice;

	/**
	 * @var array
	 */
	public $customFields;

	public function jsonSerialize()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $val){
			if(is_null($val)){
				unset($vars[$key]);
			}
		}
		return $vars;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getPartNumber(): string
	{
		return $this->partNumber;
	}

	/**
	 * @param string $partNumber
	 */
	public function setPartNumber(string $partNumber)
	{
		$this->partNumber = $partNumber;
	}

	/**
	 * @return float
	 */
	public function getUnitCost(): float
	{
		return floatval($this->unitCost);
	}

	/**
	 * @param float $unitCost
	 */
	public function setUnitCost(float $unitCost)
	{
		$this->unitCost = floatval($unitCost);
	}

	/**
	 * @return float
	 */
	public function getUnitPrice(): float
	{
		return floatval($this->unitPrice);
	}

	/**
	 * @param float $unitPrice
	 */
	public function setUnitPrice(float $unitPrice)
	{
		$this->unitPrice = floatval($unitPrice);
	}

	/**
	 * @return array
	 */
	public function getCustomFields(): array
	{
//		if(!is_array($this->customFields)){
//			$this->customFields = [];
//		}
		return $this->customFields;
	}

	/**
	 * @param array $customFields
	 */
	public function setCustomField(array $customFields)
	{
		$this->customFields = $customFields;
	}




}
