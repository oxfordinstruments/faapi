<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/24/2017
 * Time: 3:16 PM
 */

namespace Repository;

use Controller\JsonDeserializer;

define("LABOR_CONST", [
	"sbrTravel",
	"sbrRegular",
	"sbrOvertime",
	"sbrDoubletime",
	"plOvertime",
	"plDoubletime",
	"techSupport",
	"contractLabor",
	"contractTravel",
	"contractTech",
	"cost"
]);

class Labor extends JsonDeserializer implements \JsonSerializable
{
	/**
	 * @var array
	 */
	private $sbrTravel = [175.0,"SBR Travel"];

	/**
	 * @var array
	 */
	private $sbrRegular = [275.0,"SBR Regular"];

	/**
	 * @var array
	 */
	private $sbrOvertime = [412.5,"SBR Overtime"];

	/**
	 * @var array
	 */
	private $sbrDoubletime = [550.0,"SBR Doubletime"];

	/**
	 * @var array
	 */
	private $plOvertime = [375.0,"PL Overtime"];

	/**
	 * @var array
	 */
	private $plDoubletime = [500.0,"PL Doubletime"];

	/**
	 * @var array
	 */
	private $techSupport = [500.0,"Tech Support"];

	/**
	 * @var array
	 */
	private $contractLabor = [0.0,"Contract Labor"];

	/**
	 * @var array
	 */
	private $contractTravel = [0.0,"Contract Travel"];

	/**
	 * @var array
	 */
	private $contractTech = [0.0,"Contract Tech Support"];

	/**
	 * @var float
	 */
	private $cost = 30.0;

	/**
	 * @var array
	 */
	public $user;

	/**
	 * Minuets
	 * @var int
	 */
	public $quantity;

	/**
	 * @var array
	 */
	public $rate;

	public function jsonSerialize()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $val){
			if(is_null($val)){
				unset($vars[$key]);
			}
			if(in_array($key, LABOR_CONST)){
				unset($vars[$key]);
			}
		}
		return $vars;
	}

	/**
	 * @return array
	 */
	public function getSbrTravel(): array
	{
		return $this->sbrTravel;
	}

	/**
	 * @return array
	 */
	public function getSbrRegular(): array
	{
		return $this->sbrRegular;
	}

	/**
	 * @return array
	 */
	public function getSbrOvertime(): array
	{
		return $this->sbrOvertime;
	}

	/**
	 * @return array
	 */
	public function getSbrDoubletime(): array
	{
		return $this->sbrDoubletime;
	}

	/**
	 * @return array
	 */
	public function getPlOvertime(): array
	{
		return $this->plOvertime;
	}

	/**
	 * @return array
	 */
	public function getPlDoubletime(): array
	{
		return $this->plDoubletime;
	}

	/**
	 * @return array
	 */
	public function getTechSupport(): array
	{
		return $this->techSupport;
	}

	/**
	 * @return array
	 */
	public function getContractLabor(): array
	{
		return $this->contractLabor;
	}

	/**
	 * @return array
	 */
	public function getContractTravel(): array
	{
		return $this->contractTravel;
	}

	/**
	 * @return array
	 */
	public function getContractTech(): array
	{
		return $this->contractTech;
	}

	/**
	 * @return float
	 */
	public function getCost(): float
	{
		return round(floatval($this->cost), 2);
	}

	/**
	 * @param string $userUUid
	 */
	public function setUserUUID(string $userUUid)
	{
		$this->user = ["uuid" => $userUUid];
	}

	/**
	 * @return array
	 */
	public function getUser(): array
	{
		return $this->user;
	}

	/**
	 * @return int
	 */
	public function getQuantity(): int
	{
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity(int $quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return array
	 */
	public function getRate(): array
	{
		return $this->rate;
	}

	/**
	 * @param string $name
	 * @param bool $rate
	 * @throws \Exception
	 * @internal param array|float $rate
	 * @internal param float $cost
	 */
	public function setRate(string $name, $rate = false)
	{
		if(!in_array($name, LABOR_CONST)){
			throw new \Exception('Rate name is not valid for labor!');
		}

		$rateType = $this->$name;
		if($rate === false){
			$data = [
				"name" => $rateType[1],
				"rate" => $rateType[0],
				"cost" => $this->cost
			];
		}else {
			$data = [
				"name" => $rateType[1],
				"rate" => floatval($rate),
				"cost" => $this->cost
			];
		}

		$this->rate = $data;
	}

	public function getLaborConst()
	{
		return LABOR_CONST;
	}

}