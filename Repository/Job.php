<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 5:11 PM
 */

namespace Repository;

use Controller\JsonDeserializer;

/**
 * Class Job
 * @package Repository
 */
class Job extends JsonDeserializer implements \JsonSerializable
{
	/**
	 * @var string
	 */
	public $scheduledOn;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var int
	 */
	public $estDuration;

	/**
	 * @var array
	 */
	public $location;

	/**
	 * @var array
	 */
	public $contact;

	/**
	 * @var array
	 */
	public $asset;

	/**
	 * @var array
	 */
	public $jobLead;

	/**
	 * @var array
	 */
	public $customFields;

	/**
	 * @var array
	 */
	public $tasks;

	/**
	 * @var array
	 */
	public $labor;


	public function jsonSerialize()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $val){
			if(is_null($val)){
				unset($vars[$key]);
			}
		}
		return $vars;
	}

	/**
	 * @return string
	 */
	public function getScheduledOn()
	{
		return $this->scheduledOn;
	}

	/**
	 * @param string $scheduledOn
	 */
	public function setScheduledOn(string $scheduledOn)
	{
		$this->scheduledOn = $scheduledOn;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description)
	{
		$this->description = $description;
	}

	/**
	 * @return int
	 */
	public function getEstDuration(): int
	{
		return $this->estDuration;
	}

	/**
	 * @param int $estDuration
	 */
	public function setEstDuration(int $estDuration)
	{
		$this->estDuration = $estDuration;
	}

	/**
	 * @return array
	 */
	public function getLocation()
	{
		return $this->location;
	}

	/**
	 * @param string $locationUUID
	 */
	public function setLocationUUID($locationUUID)
	{
		$this->location = ["uuid" => $locationUUID];
	}

	/**
	 * @return array
	 */
	public function getContact()
	{
		return $this->contact;
	}

	/**
	 * @param string $contactUUID
	 */
	public function setContactUUID($contactUUID)
	{
		$this->contact = ["uuid" => $contactUUID];
	}

	/**
	 * @return array
	 */
	public function getAsset()
	{
		return $this->asset;
	}

	/**
	 * @return string
	 */
	public function getAssetUUID()
	{
		return $this->asset['uuid'];
	}

	/**
	 * @param string $assetUUID
	 */
	public function setAssetUUID($assetUUID)
	{
		$this->asset = ["uuid" => $assetUUID];
	}

	/**
	 * @return string
	 */
	public function getJobLeadUUID()
	{
		return $this->jobLead['uuid'];
	}

	/**
	 * @return array
	 */
	public function getJobLead()
	{
		return $this->jobLead;
	}

	/**
	 * @param string $jobLeadUUID
	 */
	public function setJobLeadUUID($jobLeadUUID)
	{
		$this->jobLead = ["uuid" => $jobLeadUUID];
	}

	/**
	 * @return array
	 */
	public function getCustomFields(): array
	{
		if(!is_array($this->customFields)){
			return null;
		}
		return $this->customFields;
	}

	/**
	 * @param array $customFields
	 */
	public function setCustomFields(array $customFields)
	{
		$this->customFields = $customFields;
	}

	/**
	 * @return Task[]
	 */
	public function getTasks(): array
	{
		return $this->tasks;
	}

	/**
	 * @param Task $task
	 */
	public function addTask(Task $task)
	{
		if(!is_array($this->tasks)){
			$this->tasks = [];
		}
		array_push($this->tasks, $task);
	}

	public function clearTasks()
	{
		$this->tasks = null;
	}

	/**
	 * @return Labor[]
	 */
	public function getLabor(): array
	{
		return $this->labor;
	}

	/**
	 * @param Labor $labor
	 */
	public function addLabor(Labor $labor)
	{
		if(!is_array($this->labor)){
			$this->labor = [];
		}
		array_push($this->labor, $labor);
	}

	public function clearLabors()
	{
		$this->labor = null;
	}


}