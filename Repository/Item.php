<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 6:14 PM
 */

namespace Repository;

use Controller\JsonDeserializer;


/**
 * Class Item
 * @package Repository
 */
class Item extends JsonDeserializer implements \JsonSerializable
{
	/**
	 * @var array
	 */
	public $item;

	/**
	 * @var int
	 */
	public $quantity;

	/**
	 * @var float
	 */
	public $unitCost;

	/**
	 * @var float
	 */
	public $unitPrice;

	/**
	 * @var array
	 */
	public $customFields;

	public function jsonSerialize()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $val){
			if(is_null($val)){
				unset($vars[$key]);
			}
		}
		return $vars;
	}

	/**
	 * @return array
	 */
	public function getUuid(): array
	{
		return $this->item;
	}

	/**
	 * @param string $uuid
	 */
	public function setUuid(string $uuid)
	{
		$this->item = ["uuid" => $uuid];
	}

	/**
	 * @return int
	 */
	public function getQuantity(): int
	{
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity(int $quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return float
	 */
	public function getUnitCost(): float
	{
		return round(floatval($this->unitCost), 2);
	}

	/**
	 * @param float $unitCost
	 */
	public function setUnitCost(float $unitCost)
	{
		$this->unitCost = round(floatval($unitCost), 2);
	}

	/**
	 * @return float
	 */
	public function getUnitPrice(): float
	{
		return round(floatval($this->unitPrice), 2);
	}

	/**
	 * @param float $unitPrice
	 */
	public function setUnitPrice(float $unitPrice)
	{
		$this->unitPrice = round(floatval($unitPrice), 2);
	}

	/**
	 * @return array
	 */
	public function getCustomFields(): array
	{
//		if(!is_array($this->customFields)){
//			$this->customFields = [];
//		}
		return $this->customFields;
	}

	/**
	 * @param array $customFields
	 */
	public function setCustomField(array $customFields)
	{
		$this->customFields = $customFields;
	}




}
