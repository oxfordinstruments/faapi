<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 6:23 PM
 */

namespace Repository;

use Controller\JsonDeserializer;

/**
 * Class Task
 * @package Repository
 */
class Task extends JsonDeserializer implements \JsonSerializable
{

	/**
	 * @var array
	 */
	public $task;

	/**
	 * @var array
	 */
	public $asset;

	/**
	 * @var array
	 */
	public $assignee;

	/**
	 * @var string
	 */
	public $note;

	/**
	 * @var float
	 */
	public $unitCost;

	/**
	 * @var float
	 */
	public $unitPrice;

	/**
	 * @var boolean
	 */
	public $done;

	/**
	 * @var array
	 */
	public $customFields;

	/**
	 * @var array
	 */
	public $items;


	public function jsonSerialize()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $val){
			if(is_null($val)){
				unset($vars[$key]);
			}
		}
		return $vars;
	}

	/**
	 * @return array
	 */
	public function getTaskUUID(): array
	{
		return $this->task['uuid'];
	}

	/**
	 * @param string $taskUUID
	 */
	public function setTaskUUID(string $taskUUID)
	{
		$this->task = ["uuid" => $taskUUID];
	}

	/**
	 * @return array
	 */
	public function getAssetUUID(): array
	{
		return $this->asset['uuid'];
	}

	/**
	 * @param string $assetUUID
	 */
	public function setAssetUUID(string $assetUUID)
	{
		$this->asset = ["uuid" => $assetUUID];
	}

	/**
	 * @return array
	 */
	public function getAssigneeUUID(): array
	{
		return $this->assignee['uuid'];
	}

	/**
	 * @param string $assigneeUUID
	 */
	public function setAssigneeUUID(string $assigneeUUID)
	{
		$this->assignee = ["uuid" => $assigneeUUID];
	}

	/**
	 * @return string
	 */
	public function getNote(): string
	{
		return $this->note;
	}

	/**
	 * @param string $note
	 */
	public function setNote(string $note)
	{
		$this->note = substr($note, 0, 4095);
	}

	/**
	 * @return float
	 */
	public function getUnitCost(): float
	{
		return round(floatval($this->unitCost), 2);

	}

	/**
	 * @param float $unitCost
	 */
	public function setUnitCost(float $unitCost)
	{
		$this->unitCost = round(floatval($unitCost), 2);
	}

	/**
	 * @return float
	 */
	public function getUnitPrice(): float
	{
		return round(floatval($this->unitPrice), 2);
	}

	/**
	 * @param float $unitPrice
	 */
	public function setUnitPrice(float $unitPrice)
	{
		$this->unitPrice = round(floatval($unitPrice), 2);
	}

	/**
	 * @return bool
	 */
	public function isDone(): bool
	{
		return round(floatval($this->done), 2);
	}

	/**
	 * @param bool $done
	 */
	public function setDone(bool $done)
	{
		$this->done = $done;
	}

	/**
	 * @return Item[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

	/**
	 * @param Item $item
	 */
	public function addItem(Item $item)
	{
		if(!is_array($this->items)) {
			$this->items = [];
		}
		array_push($this->items, $item);
	}

	public function clearItems()
	{
		$this->items = null;
	}

	/**
	 * @return array
	 */
	public function getCustomFields(): array
	{
//		if(!is_array($this->customFields)){
//			$this->customFields = [];
//		}
		return $this->customFields;
	}

	/**
	 * @param array $customFields
	 */
	public function setCustomFields(array $customFields)
	{
		$this->customFields = $customFields;
	}




}

