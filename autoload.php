<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/19/2017
 * Time: 3:46 PM
 */

require 'vendor/autoload.php';

function autoloadController($className) {
	$filename =  $className . ".php";
	if (is_readable($filename)) {
		require $filename;
	}
}

spl_autoload_register("autoloadController");
