FieldAware Create Jobs
========================
### PHP script pulling info from mysql database and creating jobs in FieldAware.

This script works in conjunction with faComplete python script.

```
Usage: run.php [-j|-i] <mode> <options>
Usage: run.php -c <options>
A mode is required when the j or i flag is used.Modes will run in the order Download, Process, Execute.
Suggested on windows to run with powershell rather than cmd!

        -s, --settings-make
                Make new settings.json file. This will overwrite the existing one.
        -j, --jobs
                Process or Execute jobs
        -i, --items
                Process or Execute items
        -d, --download-mode
                Download all of the FieldAware api tables. (Asset, Contact, Customer, Item,
                Job, Location, Task, User)
        -p, --process-mode=<number>
                Get all the jobs from the database that will be added to FieldAware and have
                an asset in the asset.json file. Jobs with errors will be saved in
                error_process_jobs.json file. Optional: Query limit and offset. The first
                value is the limit and the next is the offset. e.g. -p 50 -p 0
        -x, --execute-mode[=<number>]
                Create the new FieldAware jobs contained in jobs.json file. Optional value
                to limit the number of jobs to create e.g. -x 10
        -c, --custom-fields
                Show custom fields and create custom_fields.txt then exit.
        -v, --verbose=<Number>
                Verbose messages. -v|-vv|-vvv
        -h, --help
                Print this help message.

```